package com.zeffah.shopinv.webservice.rest;

import com.google.gson.annotations.SerializedName;
import com.zeffah.shopinv.model.Staff;

public class LoginResponse extends ServerResponse{
    @SerializedName("user")private Staff staff;

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }
}
