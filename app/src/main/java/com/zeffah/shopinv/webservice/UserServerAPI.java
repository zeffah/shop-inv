package com.zeffah.shopinv.webservice;

import com.zeffah.shopinv.callback.UserServerListener;
import com.zeffah.shopinv.model.LoginObject;
import com.zeffah.shopinv.model.Staff;
import com.zeffah.shopinv.modelservice.ShopService;
import com.zeffah.shopinv.modelservice.StaffService;
import com.zeffah.shopinv.utils.Constants;
import com.zeffah.shopinv.webservice.rest.APIClient;
import com.zeffah.shopinv.webservice.rest.APIInterface;
import com.zeffah.shopinv.webservice.rest.LoginResponse;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserServerAPI {

    public static void userLogin(String username, String password, final UserServerListener.USerLoginListener loginListener){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        LoginObject loginObject = new LoginObject(username, password);
        Call<LoginResponse> login = apiInterface.userLogin(loginObject);

        login.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();
                if (loginResponse != null) {
                    int success = loginResponse.getSuccess();
                    String message = loginResponse.getMessage();
                    if (success == Constants.RESPONSE_OK) {
                        Staff staff = loginResponse.getStaff();
                        staff.setShop(ShopService.getShopById(staff.getShopId()));
                        boolean saveStaff = StaffService.saveStaffToDB(staff);
                        if (saveStaff){
                            loginListener.onResponseSuccess(staff);
                        }else {
                            loginListener.onResponseError("Failed to save your details");
                        }
                    } else {
                        loginListener.onResponseError(message);
                    }
                } else {
                    loginListener.onResponseError("An error occurred trying to login. Please try again.");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                String msg;
                if(t instanceof SocketTimeoutException){
                    msg = "There's a server error. Please try again after some time";
                }else if (t instanceof SocketException) {
                    msg = "No internet connection";
                } else if (t instanceof UnknownHostException) {
                    msg = "Please check your network connection";
                } else {
                    msg = "Oops! An error occurred. Please try again!";
                }

                loginListener.onThrowError(msg);
            }
        });
    }


}
