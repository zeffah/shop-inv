package com.zeffah.shopinv.webservice;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.zeffah.shopinv.model.Inventory;
import com.zeffah.shopinv.modelservice.ProductService;
import com.zeffah.shopinv.modelservice.ShopService;
import com.zeffah.shopinv.webservice.rest.APIClient;
import com.zeffah.shopinv.webservice.rest.APIInterface;
import com.zeffah.shopinv.webservice.rest.InventoryResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryServerAPI {
    public static void fetchInventoriesFromServer(long shopId){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<InventoryResponse> getInventories = apiInterface.getInventories(shopId);
        getInventories.enqueue(new Callback<InventoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<InventoryResponse> call, @NonNull Response<InventoryResponse> response) {
                InventoryResponse inventoryResponse = response.body();
                Log.d("responseInv", new Gson().toJson(inventoryResponse));
                if (inventoryResponse != null){
                    List<Inventory> inventoryList = inventoryResponse.getInventories();
                    if(inventoryList != null) {
                        for (Inventory inventory : inventoryList) {
                            inventory.setShop(ShopService.getShopById(inventory.getShopId()));
                            inventory.setProduct(ProductService.getProductById(inventory.getProductId()));
                            inventory.save();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<InventoryResponse> call, @NonNull Throwable t) {
                Log.e("ThrowableError", t.getLocalizedMessage());
            }
        });
    }
}
