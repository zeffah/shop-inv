package com.zeffah.shopinv.webservice.rest;

import com.zeffah.shopinv.model.Product;

import java.util.List;

public class ProductResponse extends ServerResponse {
    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
