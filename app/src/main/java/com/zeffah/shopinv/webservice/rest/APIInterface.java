package com.zeffah.shopinv.webservice.rest;

import com.zeffah.shopinv.model.LoginObject;
import com.zeffah.shopinv.model.Sale;
import com.zeffah.shopinv.model.SaleObject;
import com.zeffah.shopinv.model.Staff;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIInterface {

    @POST("usersAPI/login")
    Call<LoginResponse> userLogin(@Body LoginObject loginObject);

    @GET("shopsAPI/shops")
    Call<ShopResponse> getAllShops();

    @GET("productsAPI/list")
    Call<ProductResponse> getAllProducts();

    @POST("inventoryAPI/list/{shop_id}")
    Call<InventoryResponse> getInventories(@Path("shop_id") long shopId);

    @POST("salesAPI/createSale")
    Call<SaleResponse> createSale(@Body SaleObject saleObject);
}
