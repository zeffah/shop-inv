package com.zeffah.shopinv.webservice;

import android.support.annotation.NonNull;
import android.util.Log;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.ModelAdapter;
import com.zeffah.shopinv.callback.ShopFetchListener;
import com.zeffah.shopinv.model.Shop;
import com.zeffah.shopinv.webservice.rest.APIClient;
import com.zeffah.shopinv.webservice.rest.APIInterface;
import com.zeffah.shopinv.webservice.rest.ShopResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShopServerAPI {
    public static void fetchAllShops(final ShopFetchListener listener) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ShopResponse> getShops = apiInterface.getAllShops();
        getShops.enqueue(new Callback<ShopResponse>() {
            @Override
            public void onResponse(@NonNull Call<ShopResponse> call, @NonNull Response<ShopResponse> response) {
                try {
                    ShopResponse shopsResponse = response.body();
                    if (shopsResponse != null){
                        List<Shop> shopList = shopsResponse.getShops();
                        if (shopList != null && !shopList.isEmpty()) {
                            for (Shop shop : shopList) {
                                ModelAdapter<Shop> shopAdapter = FlowManager.getModelAdapter(Shop.class);
                                shopAdapter.save(shop);
                            }
                        }
                        listener.onShopFetchSuccess();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ShopResponse> call, Throwable t) {
                Log.d("shops", t.toString());
                listener.onShopFetchFail(t.getMessage());
            }
        });
    }
}
