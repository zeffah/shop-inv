package com.zeffah.shopinv.webservice.rest;

import com.zeffah.shopinv.model.Inventory;

import java.util.List;

public class InventoryResponse extends ServerResponse {
    private List<Inventory> inventories;

    public List<Inventory> getInventories() {
        return inventories;
    }

    public void setInventories(List<Inventory> inventories) {
        this.inventories = inventories;
    }
}
