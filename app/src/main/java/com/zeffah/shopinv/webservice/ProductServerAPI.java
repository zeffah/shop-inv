package com.zeffah.shopinv.webservice;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.zeffah.shopinv.callback.ProductFetchListener;
import com.zeffah.shopinv.model.Product;
import com.zeffah.shopinv.webservice.rest.APIClient;
import com.zeffah.shopinv.webservice.rest.APIInterface;
import com.zeffah.shopinv.webservice.rest.ProductResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductServerAPI {
    public static void fetchProductsFromServer(final ProductFetchListener fetchListener){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ProductResponse> getProducts = apiInterface.getAllProducts();
        getProducts.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProductResponse> call, @NonNull Response<ProductResponse> response) {
                ProductResponse productResponse = response.body();
                Log.d("responseProd", new Gson().toJson(productResponse));
                if (productResponse != null){
                    List<Product> productList = productResponse.getProducts();
                    for (Product product: productList){
                        product.save();
                    }
                    fetchListener.onProductsFetchSuccess();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductResponse> call, @NonNull Throwable t) {
                fetchListener.onProductsFetchFail(t.getLocalizedMessage());
            }
        });
    }
}