package com.zeffah.shopinv.webservice.rest;

import com.zeffah.shopinv.model.Shop;

import java.util.List;

public class ShopResponse {
    private List<Shop> shops;

    public List<Shop> getShops() {
        return shops;
    }

    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }
}
