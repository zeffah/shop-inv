package com.zeffah.shopinv.webservice;

import android.support.annotation.NonNull;

import com.zeffah.shopinv.callback.SaleCreateListener;
import com.zeffah.shopinv.model.Sale;
import com.zeffah.shopinv.model.SaleObject;
import com.zeffah.shopinv.webservice.rest.APIClient;
import com.zeffah.shopinv.webservice.rest.APIInterface;
import com.zeffah.shopinv.webservice.rest.SaleResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesServerAPI {
    public static void sendSaleToServer(final Sale sale, final SaleCreateListener createListener){
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        SaleObject object = new SaleObject(sale.getSaleAmount(), sale.getStaff().getStaffId(), sale.shopId, sale.saleItems);
        Call<SaleResponse> createSaleOnServer = apiInterface.createSale(object);
        createSaleOnServer.enqueue(new Callback<SaleResponse>() {
            @Override
            public void onResponse(@NonNull Call<SaleResponse> call, @NonNull Response<SaleResponse> response) {
                SaleResponse res = response.body();
                createListener.onSentToServer(res, sale);

            }

            @Override
            public void onFailure(@NonNull Call<SaleResponse> call, @NonNull Throwable t) {

            }
        });
    }
}
