package com.zeffah.shopinv.callback;

public interface ProductFetchListener {
    void onProductsFetchSuccess();
    void onProductsFetchFail(String message);
}
