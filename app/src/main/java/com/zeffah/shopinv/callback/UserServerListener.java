package com.zeffah.shopinv.callback;

import com.zeffah.shopinv.model.Staff;

public class UserServerListener {
    public interface USerLoginListener {
        void onResponseSuccess(Staff staff);
        void onResponseError(String msg);
        void onThrowError(String msg);
    }
}
