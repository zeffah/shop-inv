package com.zeffah.shopinv.callback;

public interface ShopFetchListener {
    void onShopFetchSuccess();
    void onShopFetchFail(String message);
}
