package com.zeffah.shopinv.callback;

import android.view.View;

import com.zeffah.shopinv.model.SaleItem;

public interface AddToSaleListener {
    void onItemAddedToSale(View view, SaleItem saleItem);
}
