package com.zeffah.shopinv.callback;

import com.zeffah.shopinv.model.Sale;
import com.zeffah.shopinv.webservice.rest.SaleResponse;

import org.json.JSONObject;

import retrofit2.Response;

public interface SaleCreateListener {
    void onCreateSale(Sale sale);
    void onSentToServer(SaleResponse response, Sale sale);
}
