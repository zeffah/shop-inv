package com.zeffah.shopinv.common;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.wang.avi.AVLoadingIndicatorView;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.utils.BadgeDrawable;

public class PublicMethods {

    public static void openFragment(Context context, Fragment nextFragment){
        FragmentTransaction transaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.app_container, nextFragment, nextFragment.getClass().getSimpleName());
        transaction.addToBackStack(nextFragment.getClass().getSimpleName()).commit();
    }

    public static void openDialogFragment(Context context, DialogFragment dialogFragment){
        FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
        dialogFragment.show(fragmentManager, dialogFragment.getClass().getSimpleName());
    }

    public static void newActivityStart(Context context, Class mClass, AVLoadingIndicatorView loadingIndicatorView){
        Intent intent = new Intent(context, mClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        if (loadingIndicatorView != null) loadingIndicatorView.smoothToHide();
        ((AppCompatActivity)context).finish();
    }

    public static TextDrawable getTextDrawable(String initial) {
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        int color = generator.getColor(initial);
        return TextDrawable.builder()
                .beginConfig()
                .fontSize(22) /* size in px */
                .endConfig()
                .buildRound(initial, color);
    }

    public static void changeTitleStyle(Context context, Dialog dialog) {
        TextView title = (TextView) dialog.findViewById(android.R.id.title);
        if (title != null) {
            title.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            title.setSingleLine(false);
        }
        int titleDividerId = context.getResources().getIdentifier("titleDivider", "id", "android");
        View titleDivider = dialog.findViewById(titleDividerId);
        if (titleDivider != null)
            titleDivider.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
    }

    public static String getFirstLetters(String text) {
        String firstLetters = "";
        String returnedText = "";
        text = text.replaceAll("[.,]", ""); // Replace dots, etc (optional)
        try {
            if (text != null && text.length() > 0) {
                for (String s : text.split(" ")) {
                    firstLetters += s.charAt(0);
                }
                returnedText = firstLetters;
            }
        } catch (StringIndexOutOfBoundsException e) {
            returnedText = text.substring(0, 2);
        }
        return returnedText;
    }

    public static void setActionBarTitle(Context context, String title, boolean setDisplayHomeAsUpEnabled){
        ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();
//        SpannableString s = new SpannableString(title);
//        s.setSpan(new TypefaceSpan("gotham_book.otf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (actionBar != null) {
            actionBar.setTitle(title);
            if (!setDisplayHomeAsUpEnabled){
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
        }
    }

    public static void openPageFragment(Context context, Fragment pageFragment, boolean isHome){
        Toolbar toolbar = ((AppCompatActivity)context).findViewById(R.id.toolbar);
        FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
        String className = pageFragment.getClass().getSimpleName();
        ft.replace(R.id.app_container, pageFragment, className);
        if (!isHome){
            ft.addToBackStack(className);
        }
        ft.commit();
        enableOrDisableToolbarBack(context, toolbar, isHome);
    }

    public static AlertDialog actionProgressDialog(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setView(R.layout.loading_indicator_dialog);
        AlertDialog alertDialog = builder.create();
        Window viewWindow = alertDialog.getWindow();
        if (viewWindow != null){
            viewWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        return alertDialog;
    }

    private static void enableOrDisableToolbarBack(final Context context, final Toolbar toolbar, final boolean isHomePage) {
        ((AppCompatActivity)context).getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                ActionBar actionBar = ((AppCompatActivity)context).getSupportActionBar();
                if (((AppCompatActivity)context).getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    if (isHomePage) {
                        if (actionBar != null)
                            actionBar.setDisplayHomeAsUpEnabled(false);
                    } else {
                        if (actionBar != null) {
                            actionBar.setDisplayHomeAsUpEnabled(true);
                        }
                        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((AppCompatActivity)context).onBackPressed();
                            }
                        });
                    }
                } else {
                    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(false);
                }
            }
        });
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {
        BadgeDrawable badge;
        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }
        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }
}
