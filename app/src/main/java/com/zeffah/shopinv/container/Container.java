package com.zeffah.shopinv.container;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.zeffah.shopinv.R;
import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.pages.DashboardFragment;
import com.zeffah.shopinv.pages.LoginFragment;
import com.zeffah.shopinv.pages.ShopInventoryFragment;

public class Container extends AppCompatActivity {
    private boolean isHomePage = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        //starting home page
        openPageFragment(new DashboardFragment(), true);
        Toolbar toolbar;

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void openPageFragment(Fragment pageFragment, boolean isHome){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        String className = pageFragment.getClass().getSimpleName();
        ft.replace(R.id.app_container, pageFragment, className);
        if (!isHome){
            ft.addToBackStack(className);
        }
        ft.commit();
    }
}
