package com.zeffah.shopinv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeffah.shopinv.R;
import com.zeffah.shopinv.model.Inventory;
import com.zeffah.shopinv.model.Product;
import com.zeffah.shopinv.model.SaleItem;

import java.util.List;

public class SaleItemsListAdapter extends RecyclerView.Adapter<SaleItemsListAdapter.ViewHolder> {
    private List<SaleItem> saleItemList;
    private SaleItemsListAdapterListener adapterListener;

    public SaleItemsListAdapter(List<SaleItem> saleItemList, SaleItemsListAdapterListener adapterListener) {
        this.saleItemList = saleItemList;
        this.adapterListener = adapterListener;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.sale_list_item_single, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final SaleItem saleItem = saleItemList.get(position);
        if (saleItem != null) {
            Inventory inventory = saleItem.getInventory();
            final Product product = inventory.getProduct();
            holder.txtProductName.setText(String.valueOf(product.getProductName() + " - " + product.getProductType()));
            holder.txtProductRetail.setText(String.valueOf("Ksh."+saleItem.getSellingPrice()));
            holder.txtInventoryQty.setText(String.valueOf(saleItem.getItemQuantity()));
            holder.imgReduceQty.setOnClickListener(new ViewClick(holder, saleItem, holder.getAdapterPosition()));
            holder.imgAddQty.setOnClickListener(new ViewClick(holder, saleItem, holder.getAdapterPosition()));
        }
    }

    @Override
    public int getItemCount() {
        return saleItemList.size();
    }

    public void removeItem(int position) {
        saleItemList.remove(position);
        notifyDataSetChanged();
    }

    private class ViewClick implements View.OnClickListener {
        ViewHolder viewHolder;
        int position;
        SaleItem saleItem;
        ViewClick(ViewHolder viewHolder, SaleItem saleItem, int position){
            this.viewHolder = viewHolder;
            this.position = position;
            this.saleItem = saleItem;
        }
        @Override
        public void onClick(View v) {
            int quantity = Integer.parseInt(viewHolder.txtInventoryQty.getText().toString().trim());
            if (v == this.viewHolder.imgAddQty){
                quantity += 1;
                saleItem.setItemQuantity(quantity);
                viewHolder.txtInventoryQty.setText(String.valueOf(quantity));
                adapterListener.onItemQuantityChange(saleItemList);
                return;
            }
            if (v == this.viewHolder.imgReduceQty){
                if (quantity > 1){
                    quantity -=1;
                    saleItem.setItemQuantity(quantity);
                    viewHolder.txtInventoryQty.setText(String.valueOf(quantity));
                    adapterListener.onItemQuantityChange(saleItemList);
                }
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private View view;
        private ImageView imgAddQty, imgReduceQty;
        private TextView txtProductName, txtProductRetail, txtInventoryQty;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            txtProductName = this.view.findViewById(R.id.txt_product_name);
            txtProductRetail = this.view.findViewById(R.id.txt_selling_price);
            txtInventoryQty = this.view.findViewById(R.id.sale_item_qty);
            imgAddQty = this.view.findViewById(R.id.img_add_qty);
            imgReduceQty = this.view.findViewById(R.id.img_reduce_qty);
        }
    }

    public interface SaleItemsListAdapterListener {
        void onItemQuantityChange(List<SaleItem> saleItems);
    }
}
