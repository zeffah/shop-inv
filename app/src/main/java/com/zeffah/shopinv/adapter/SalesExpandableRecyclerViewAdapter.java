package com.zeffah.shopinv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hendraanggrian.widget.ExpandableItem;
import com.hendraanggrian.widget.ExpandableRecyclerView;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.model.Sale;
import com.zeffah.shopinv.model.SaleItem;
import com.zeffah.shopinv.model.Shop;
import com.zeffah.shopinv.model.Staff;
import com.zeffah.shopinv.modelservice.ShopService;
import com.zeffah.shopinv.ui.ListViewCustom;
import com.zeffah.shopinv.utils.DateTimeMethods;
import com.zeffah.shopinv.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SalesExpandableRecyclerViewAdapter extends ExpandableRecyclerView.Adapter<SalesExpandableRecyclerViewAdapter.ViewHolder> {
    private Context context = null;
    private List<Sale> salesList;
    private SalesListAdapterListener listener;

    private SparseBooleanArray selectedItems;
    // array used to perform multiple animation at once
    private SparseBooleanArray animationItemsIndex;
    private boolean reverseAllAnimations = false;
    private static int currentSelectedIndex = -1;

    public SalesExpandableRecyclerViewAdapter(@NonNull LinearLayoutManager linearLayoutManager, List<Sale> salesList, SalesListAdapterListener listener) {
        super(linearLayoutManager);
        this.salesList = salesList;
        this.listener = listener;
        this.selectedItems = new SparseBooleanArray();
        this.animationItemsIndex = new SparseBooleanArray();
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.sale_item_expandable_row, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        Sale sale = salesList.get(position);
        if (sale != null) {
            Staff sessionStaff = SessionManager.getSessionStaff(context);
            Shop shop = ShopService.getShopById(sessionStaff.getShopId());
            String saleNo = String.format(Locale.getDefault(), "ST%03d-SP%03d-%05d", sessionStaff.getStaffId(), shop.getShopId(), sale.getSaleId());
            (holder).txtSaleName.setText(saleNo);
            (holder).txtSaleTotal.setText(String.valueOf("Ksh."+sale.getSaleAmount()));
            (holder).txtSaleType.setText(String.valueOf("Cash Sale"));
            (holder).txtCreateDate.setText(DateTimeMethods.formatDateFromTimestamp(String.valueOf(sale.getCreatedAt())));
            holder.txtTitle.setVisibility(View.GONE);

            setOrderItemsList(sale, holder.listOrderItems, position);

            holder.txtDrawable.setText(String.format(Locale.getDefault(), "%04d", sale.getSaleId()));
            holder.imgTextDrawableFront.setImageResource(R.drawable.round_image);
            holder.imgIconDrawableBack.setVisibility(View.GONE);

            try {
                applyIconAnimation(holder, position);
                applyClickEvents(holder, sale, position);
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return salesList.size();
    }

    private void applyClickEvents(final ViewHolder holder, final Sale sale, final int position) {

        holder.iconContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onIconClicked(position, sale);
            }
        });

        holder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onItemRowLongClicked(position, sale);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                return true;
            }
        });

        holder.imgItemsExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void applyIconAnimation(ViewHolder holder, int position) {
        if (selectedItems.get(position, false)) {
            holder.viewContainerFront.setVisibility(View.GONE);
            resetIconYAxis(holder.imgIconDrawableBack);
            holder.imgIconDrawableBack.setVisibility(View.VISIBLE);
            holder.imgIconDrawableBack.setAlpha(1);
            if (currentSelectedIndex == position) {
//                FlipAnimator.flipView(context, holder.imgIconDrawableBack, holder.viewContainerFront, true);
                resetCurrentIndex();
            }
        } else {
            holder.imgIconDrawableBack.setVisibility(View.GONE);
            resetIconYAxis(holder.viewContainerFront);
            holder.viewContainerFront.setVisibility(View.VISIBLE);
            holder.viewContainerFront.setAlpha(1);
            if ((reverseAllAnimations && animationItemsIndex.get(position, false)) || currentSelectedIndex == position) {
//                FlipAnimator.flipView(context, holder.imgIconDrawableBack, holder.viewContainerFront, false);
                resetCurrentIndex();
            }
        }
    }

    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            animationItemsIndex.delete(pos);
        } else {
            selectedItems.put(pos, true);
            animationItemsIndex.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        reverseAllAnimations = true;
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        salesList.remove(position);
        resetCurrentIndex();
    }

    public void removeItem(int position) {
        salesList.remove(position);
        notifyDataSetChanged();
        resetCurrentIndex();
    }

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View view, saleView, saleItemsView, viewContainerFront, iconContainer;
        ExpandableItem rowView;
        ImageView imgIconDrawableBack, imgTextDrawableFront, imgItemsExpand;
        TextView txtSaleName, txtSaleType, txtSaleTotal, txtCreateDate, txtDrawable, txtTitle;
        ListViewCustom listOrderItems;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            this.rowView = this.view.findViewById(R.id.row);
            this.saleView = this.rowView.getHeaderLayout();
            this.txtSaleName = this.saleView.findViewById(R.id.txt_sale_number);
            this.txtSaleType = this.saleView.findViewById(R.id.txt_sale_type);
            this.txtSaleTotal = this.saleView.findViewById(R.id.txt_sale_total_amount);
            this.txtCreateDate = this.saleView.findViewById(R.id.txt_sale_date);
            this.txtDrawable = this.saleView.findViewById(R.id.txt_initials);
            this.viewContainerFront = this.saleView.findViewById(R.id.icon_container_front);
            this.imgTextDrawableFront = this.saleView.findViewById(R.id.img_icon_drawable_front);
            this.imgItemsExpand= this.saleView.findViewById(R.id.img_sale_items_expand);
            this.imgIconDrawableBack = this.saleView.findViewById(R.id.img_icon_drawable_back);
            this.iconContainer = this.saleView.findViewById(R.id.icon_container);

            this.saleItemsView = this.rowView.getContentLayout();
            this.listOrderItems = this.saleItemsView.findViewById(R.id.sale_items_list_view);
            this.txtTitle = this.saleItemsView.findViewById(R.id.txt_list_title);
        }
    }

    private void setOrderItemsList(Sale sale, ListViewCustom listView, int orderPosition) {
        List<SaleItem> saleItemsList = sale.getSaleItems();
        listView.setAdapter(new SaleItemsAdapter(context, R.layout.sale_made_list_items, saleItemsList, orderPosition));
    }

    // As the views will be reused, sometimes the icon appears as
    // flipped because older view is reused. Reset the Y-axis to 0
    private void resetIconYAxis(View view) {
        if (view.getRotationY() != 0) {
            view.setRotationY(0);
        }
    }

    public void resetAnimationIndex() {
        reverseAllAnimations = false;
        animationItemsIndex.clear();
    }

    public interface SalesListAdapterListener {
        void onIconClicked(int position, Sale sale);

        void onItemRowLongClicked(int position, Sale sale);

        void onOrderStatusIconClick(int position, Sale sale);

        void onOrderApproveIconClick(int position, Sale sale);
    }
}
