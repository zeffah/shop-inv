package com.zeffah.shopinv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.zeffah.shopinv.R;
import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.model.Inventory;
import com.zeffah.shopinv.model.Product;
import com.zeffah.shopinv.model.SaleItem;
import com.zeffah.shopinv.modelservice.InventoryService;
import com.zeffah.shopinv.modelservice.ProductService;

import java.util.List;

/*
 * Created by zeffah on 11/22/17.
 */

public class SaleItemsAdapter extends ArrayAdapter<SaleItem> {
    private List<SaleItem> saleItemList;
    private Context context;
    private int resource;
    private int orderPosition;
    public SaleItemsAdapter(@NonNull Context context, int resource, @NonNull List<SaleItem> saleItemList, int orderPosition) {
        super(context, resource, saleItemList);
        this.saleItemList = saleItemList;
        this.context = context;
        this.resource = resource;
        this.orderPosition = orderPosition;
    }

    @Override
    public int getCount() {
        return saleItemList.size();
    }

    @Nullable
    @Override
    public SaleItem getItem(int position) {
        return saleItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null){
            rowView = LayoutInflater.from(context).inflate(resource, parent, false);
            ViewHolder viewHolder = new ViewHolder(rowView);
            rowView.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();
        SaleItem saleItem = saleItemList.get(position);
        if (saleItem != null){
            long inventoryId = saleItem.getInventory().getInventoryId();
            Inventory inventory = InventoryService.getInventoryById(inventoryId);
            Product product = ProductService.getProductById(inventory.getProduct().getProductId());
            String productNameType;
            productNameType = product.getProductName()+" ("+product.getProductType()+")";
            holder.txtProductName.setText(String.valueOf(productNameType));
            holder.txtQuantityOrdered.setText(String.valueOf(saleItem.getItemQuantity()));
            holder.txtProductTotal.setText(String.valueOf("Ksh."+saleItem.getItemAmount()));
            holder.txtItemNumber.setText(String.valueOf(saleItem.getItemQuantity()));
            holder.txtManufacturer.setText(product.getProductManufacturer());
            holder.txtPrice.setText(String.valueOf("Ksh."+saleItem.getSellingPrice()));
        }
        return rowView;
    }

    static class ViewHolder {
        View view;
        TextView txtProductName, txtQuantityOrdered, txtItemNumber, txtPrice, txtManufacturer, txtProductTotal;
        public ViewHolder(View view){
           this.view = view;
           this.txtProductName = this.view.findViewById(R.id.txt_product_name_type);
           this.txtQuantityOrdered = this.view.findViewById(R.id.txt_quantity_ordered);
           this.txtItemNumber = this.view.findViewById(R.id.txt_item_number);
           this.txtPrice = this.view.findViewById(R.id.txt_buying_price);
           this.txtProductTotal = this.view.findViewById(R.id.txt_product_total);
           this.txtManufacturer = this.view.findViewById(R.id.txt_product_manufacturer);
        }
    }
}
