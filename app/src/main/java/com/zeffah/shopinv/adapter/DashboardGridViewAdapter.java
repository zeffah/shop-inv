package com.zeffah.shopinv.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeffah.shopinv.R;
import com.zeffah.shopinv.model.Dashboard;

import java.util.List;

public class DashboardGridViewAdapter extends BaseAdapter {
    private Activity activity;
    private List<Dashboard> dashList;

    private static class ViewHolder {
        ImageView imgViewFlag;
        TextView txtViewTitle;
    }

    public DashboardGridViewAdapter(Activity activity, List<Dashboard> dashList) {
        this.activity = activity;
        this.dashList = dashList;
    }
    @Override
    public int getCount() {
        return this.dashList.size();
    }
    @Override
    public Dashboard getItem(int position) {
        return dashList.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder view;
        LayoutInflater inflater = this.activity.getLayoutInflater();
        if (convertView == null) {
            view = new ViewHolder();
            convertView = inflater.inflate(R.layout.dashboard_icon_item, parent, false);
            view.txtViewTitle = convertView.findViewById(R.id.txt_label);
            view.imgViewFlag = convertView.findViewById(R.id.image_icon);
            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }
        Dashboard mDash = dashList.get(position);
        view.txtViewTitle.setText(mDash.getLabel());
        view.imgViewFlag.setImageDrawable(mDash.getIcons());
        return convertView;
    }
}
