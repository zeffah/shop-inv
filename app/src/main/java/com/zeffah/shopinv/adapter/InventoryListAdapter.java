package com.zeffah.shopinv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.model.Inventory;
import com.zeffah.shopinv.model.Product;
import com.zeffah.shopinv.modelservice.ProductService;

import java.util.List;
import java.util.Locale;

public class InventoryListAdapter extends RecyclerView.Adapter<InventoryListAdapter.ViewHolder> {
    private Context context;
    private List<Inventory> inventoryList;
    private InventoryListAdapterListener adapterListener;

    public InventoryListAdapter(List<Inventory> inventoryList, InventoryListAdapterListener adapterListener) {
        this.inventoryList = inventoryList;
        this.adapterListener = adapterListener;
    }

    @Override @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.inventory_single_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Inventory inventory = inventoryList.get(position);
        if (inventory != null) {
            final Product product = ProductService.getProductById(inventory.getProduct().getProductId());
            inventory.setProduct(product);
            if (product.getProductType().length() > 10){
                holder.txtProductType.setVisibility(View.VISIBLE);
                holder.txtProductType.setText(product.getProductType());
                (holder).txtProductName.setText(product.getProductName());
            }else {
                holder.txtProductType.setVisibility(View.GONE);
                (holder).txtProductName.setText(String.valueOf(product.getProductName()+" - "+product.getProductType()));
            }
            (holder).txtManufacturerName.setText(product.getProductManufacturer());
            holder.txtProductRetail.setText(String.valueOf(inventory.getSellingPrice()));
            holder.txtProductWholesale.setText(String.valueOf(inventory.getBuyingPrice()));
            holder.txtInventoryQty.setText(String.valueOf(inventory.getInventoryQuantity()));

            TextDrawable drawable = PublicMethods.getTextDrawable(PublicMethods.getFirstLetters(product.getProductName().toUpperCase(Locale.getDefault())));
            holder.imgInitials.setImageDrawable(drawable);

            holder.imgAddToSellBasket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapterListener.onIconClicked(holder.getAdapterPosition(), inventory);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return inventoryList.size();
    }

    public void removeData(int position) {
        inventoryList.remove(position);
    }

    public void removeItem(int position) {
        inventoryList.remove(position);
        notifyDataSetChanged();
    }

    public void reloadList(List<Inventory> list){
        inventoryList.clear();
        inventoryList.addAll(list);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        ImageView imgOpenOptions, imgAddToSellBasket, imgInitials;
        TextView txtProductName, txtManufacturerName, txtCategoryName, txtProductType, txtProductRetail, txtProductWholesale, txtInventoryQty;

        ViewHolder(View view) {
            super(view);
            this.view = view;
            txtProductName = this.view.findViewById(R.id.txt_inventory_product_name);
            txtManufacturerName = this.view.findViewById(R.id.txt_inventory_product_manufacturer);
            txtProductType = this.view.findViewById(R.id.txt_inventory_product_type);
            txtProductRetail = this.view.findViewById(R.id.txt_inventory_product_selling_price);
            txtProductWholesale = this.view.findViewById(R.id.txtInventory_main_wholeSale_price);
            txtInventoryQty = this.view.findViewById(R.id.txt_inventory_product_instock);
            imgInitials = this.view.findViewById(R.id.img_inventory_main_initials);
            imgAddToSellBasket = this.view.findViewById(R.id.img_make_sale_action);
        }
    }

    public interface InventoryListAdapterListener {
        void onIconClicked(int position, Inventory inventory);

        void onItemRowLongClicked(int position,  Inventory inventory);

        void onOrderStatusIconClick(int position, Inventory inventory);

        void onOrderApproveIconClick(int position,  Inventory inventory);
    }
}
