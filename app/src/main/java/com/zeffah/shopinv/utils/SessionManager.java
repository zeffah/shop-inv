package com.zeffah.shopinv.utils;

import android.content.Context;

import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.model.Staff;
import com.zeffah.shopinv.pages.LoginFragment;

public class SessionManager {
    public static boolean isUserLoggedIn(Context context) {
        return new PreferenceManager(context).isLoggedIn();
    }

    public static boolean setUserSession(Context context, Staff staff) {
       return new PreferenceManager(context).setUserInfo(staff, true);
    }

    public static void logout(Context context){
        if (new PreferenceManager(context).userLogout()){
            PublicMethods.newActivityStart(context, LoginFragment.class, null);
        }
    }

    public static Staff getSessionStaff(Context context){
        return new PreferenceManager(context).getUserInfo();
    }

    public static long getStaffShopId(Context context){
        return getSessionStaff(context).getShopId();
    }
}
