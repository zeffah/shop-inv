package com.zeffah.shopinv.utils;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

/**
 * Created by zeffah on 1/13/17.
 **/

public class MyXAxisFormatter implements IAxisValueFormatter {
    private ArrayList<String> values;

    public MyXAxisFormatter(ArrayList<String> values){
     this.values = values;
    }
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        String returnValue;
        if (value < 0){
            returnValue = "";
        }else {
            returnValue = values.get((int)value);
        }
        return returnValue;
    }
}
