package com.zeffah.shopinv.utils;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Patrick on 15/06/2016.
 */
public class DateTimeMethods {
    public static Date nextDayStart(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DATE, 1);
        return cal.getTime();
    }

    public static Date getDateFromStringDate(String strDate){
        DateFormat formatter;
        Date date = null;
        try {
            formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            date = formatter.parse(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getDateFromAString(String str_date) {
        DateFormat formatter;
        Date date = null;
        try {
            formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            date = formatter.parse(str_date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String formatDateFromTimestamp(String timestamp) {
        long floatTimeStamp;
        String formatedDate;
        if (TextUtils.isEmpty(timestamp)) {
            formatedDate = "Not Set";
        } else {
            floatTimeStamp = Long.parseLong(timestamp);
            Date dateObject = new Date(floatTimeStamp * 1000);
            formatedDate = new SimpleDateFormat("EE, dd-MM-yyyy").format(dateObject);
        }
        return formatedDate;
    }

    public static String formatDateFromTimestamp(long timestamp) {
        long floatTimeStamp;
        String formattedDate;
            floatTimeStamp = timestamp;
            Date dateObject = new Date(floatTimeStamp * 1000);
            formattedDate = new SimpleDateFormat("dd-MM-yyyy").format(dateObject);
        return formattedDate;
    }

    public static String formatDateFromTimestamp1(String timestamp) {
        long floatTimeStamp;
        String formattedDate;
        if (TextUtils.isEmpty(timestamp)) {
            formattedDate = "Not Set";
        } else {
            floatTimeStamp = Long.parseLong(timestamp);
            Date dateObject = new Date(floatTimeStamp * 1000);
            formattedDate = new SimpleDateFormat("EE, dd-MM-yyyy HH:mm:ss").format(dateObject);
        }
        return formattedDate;
    }

    public static String fileNameFormattedFromDate(String timestamp) {
        long floatTimeStamp;
        String formattedDate;
        if (TextUtils.isEmpty(timestamp)) {
            formattedDate = "Not Set";
        } else {
            floatTimeStamp = Long.parseLong(timestamp);
            Date dateObject = new Date(floatTimeStamp * 1000);
            formattedDate = new SimpleDateFormat("EE-dd-MM-yyyy-HH-mm-ss").format(dateObject);
        }
        return formattedDate;
    }

    public static String formatDateObjectToString(Date dateObject) {
        String formatedDate = "null";
        if (dateObject != null) {
            formatedDate = new SimpleDateFormat("EE,dd-MM-yyyy HH:mm:ss").format(dateObject);
        }
        return formatedDate;
    }

    public static long getCurrentTime() {
        Long tsLong = System.currentTimeMillis() / 1000;
        return (long) tsLong;
    }

    public static Date getFirstDateOfCurrentMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static long firstDayOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis() / 1000;
    }

    public static long lastDayOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 59);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTimeInMillis() / 1000;
    }

    public static long lastDayOfPreviousMonth(){
        Calendar mCalendar = Calendar.getInstance();
        mCalendar.add(Calendar.MONTH, -1);
        mCalendar.set(Calendar.DAY_OF_MONTH, mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return mCalendar.getTimeInMillis()/1000;
    }

    public static long firstDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //return calendar.getTime();
        return calendar.getTimeInMillis() / 1000;
    }

    public static long lastDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        calendar.add(Calendar.DAY_OF_WEEK, -1);
        calendar.add(Calendar.WEEK_OF_YEAR, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 59);
        // return calendar.getTime();
        return calendar.getTimeInMillis() / 1000;
    }

    public static long firstDayOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        // return calendar.getTime();
        return calendar.getTimeInMillis() / 1000;
    }

    public static long lastDayOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, 31);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 59);
        // return calendar.getTime();
        return calendar.getTimeInMillis() / 1000;
    }

    public static long getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //return calendar.getTime();
        return calendar.getTimeInMillis() / 1000;
    }

    public static long getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 59);
        return calendar.getTimeInMillis() / 1000;
        //return calendar.getTime();
    }
}
