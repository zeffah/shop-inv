package com.zeffah.shopinv.utils;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

public class MyCustomYAxisFormatter extends PercentFormatter {
        private DecimalFormat mFormat;

        private MyCustomYAxisFormatter(DecimalFormat mFormat) {
            this.mFormat = mFormat;
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return mFormat.format(value) + " %";
        }
    }