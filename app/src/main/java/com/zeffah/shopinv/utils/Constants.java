package com.zeffah.shopinv.utils;

public class Constants {
    public static final int RESPONSE_OK = 1;

    public static final String MY_SALES_LIST = "My Sales";
    public static final String MY_INVENTORY = "Inventory";
    public static final String MAKE_SALE = "Make Sale";
    public static final String REPORTS = "Reports";
}
