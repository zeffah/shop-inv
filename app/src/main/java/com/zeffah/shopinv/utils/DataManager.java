package com.zeffah.shopinv.utils;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.zeffah.shopinv.model.Inventory;
import com.zeffah.shopinv.model.Product;

import java.util.ArrayList;
import java.util.List;

public class DataManager {

    public static List<Inventory> fetchProducts() {

        List<Inventory> inventoryList = new ArrayList<>();
        Product product = new Product();
        product.setProductType("20kg");
        product.setProductCategory("Fungicide");
        product.setProductManufacturer("Agrib Africa Ltd");
        product.setProductName("Vital Dry");

        Inventory inventory = new Inventory();
        inventory.setBuyingPrice(200);
        inventory.setSellingPrice(250);
        inventory.setInventoryQuantity(90);
        inventory.setProduct(product);

        Product product1 = new Product();
        product1.setProductType("5kg");
        product1.setProductCategory("Seeds");
        product1.setProductManufacturer("Kenya seed Co.");
        product1.setProductName("H504 maize");

        Inventory inventory1 = new Inventory();
        inventory1.setProduct(product1);
        inventory1.setSellingPrice(120);
        inventory1.setBuyingPrice(100);
        inventory1.setInventoryQuantity(23);
        inventoryList.add(inventory1);
        inventoryList.add(inventory);

        return SQLite.select().from(Inventory.class).queryList();
    }
}
