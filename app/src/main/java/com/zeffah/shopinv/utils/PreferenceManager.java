package com.zeffah.shopinv.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.zeffah.shopinv.model.Staff;

public class PreferenceManager {
    private static final String USER_SESSION_PREFS = "USER_SESSION_PREFS";
    private boolean isLoggedIn = false;
    private static final String USER_LOGGED_KEY = "user_logged_in";
    private static final String USERNAME_KEY = "USERNAME";
    private static final String PASSWORD_KEY = "PASSWORD";
    private static final String SHOP_ID_KEY = "SHOP_ID";
    private static final String STAFF_ID_KEY = "STAFF_ID";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor user_prefs_editor;

    public PreferenceManager(Context context){
        this.sharedPreferences = context.getSharedPreferences(USER_SESSION_PREFS, Context.MODE_PRIVATE);
    }
    public boolean isLoggedIn(){
        return sharedPreferences.getBoolean(USER_LOGGED_KEY, isLoggedIn);
    }

    public boolean userLogout() {
        user_prefs_editor = this.sharedPreferences.edit();
        user_prefs_editor.clear();
        if(user_prefs_editor.commit()) {
            user_prefs_editor.apply();
            return true;
        }
        return false;
    }

    public boolean setUserInfo(Staff staff, boolean loggedIn) {
        user_prefs_editor = this.sharedPreferences.edit();
        user_prefs_editor.putString(USERNAME_KEY, staff.getUsername());
        user_prefs_editor.putString(PASSWORD_KEY, staff.getPassword());
        user_prefs_editor.putInt(SHOP_ID_KEY, staff.getShopId());
        user_prefs_editor.putLong(STAFF_ID_KEY, staff.getStaffId());
        user_prefs_editor.putBoolean(USER_LOGGED_KEY, loggedIn);
        if(user_prefs_editor.commit()) {
            user_prefs_editor.apply();
            return true;
        }
        return false;
    }

    public Staff getUserInfo(){
        Staff staff = new Staff();
        staff.setShopId(this.sharedPreferences.getInt(SHOP_ID_KEY, -1));
        staff.setUsername(this.sharedPreferences.getString(USERNAME_KEY, ""));
        staff.setStaffId(this.sharedPreferences.getLong(STAFF_ID_KEY, -1));
        return staff;
    }
}
