package com.zeffah.shopinv.dialog;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.callback.AddToSaleListener;
import com.zeffah.shopinv.model.Inventory;
import com.zeffah.shopinv.model.SaleItem;

public class CustomDialog {

    public static CFAlertDialog addItemToSaleList(final Context context, Inventory inventory, CFAlertDialog.CFAlertStyle cfAlertStyle, AddToSaleListener saleListener) {
        View view = LayoutInflater.from(context).inflate(R.layout.sell_item_layout, null, false);
        final EditText edtQuantity = view.findViewById(R.id.quantity), edtPrice = view.findViewById(R.id.sold_at);
        Button btnAddToSale = view.findViewById(R.id.btn_add_to_sale_list);
        CFAlertDialog.Builder dialogBuilder = new CFAlertDialog.Builder(context)
                .setFooterView(view)
                .setTitle("Sell " + inventory.getProduct().getProductName() + " - " + inventory.getProduct().getProductType())
                .setMessage(String.valueOf("Actual Available Stock: " + inventory.getInventoryQuantity() + " Units\nRecommended Retail Price: Ksh." + inventory.getSellingPrice()))
                .setDialogStyle(cfAlertStyle)
                .setTextGravity(Gravity.START);

        final CFAlertDialog dialog = dialogBuilder.create();
        edtPrice.setText(String.valueOf(inventory.getSellingPrice()));

        btnAddToSale.setOnClickListener(new OnclickEvent(saleListener, edtQuantity, edtPrice, inventory, dialog));

        return dialog;
    }

    public static CFAlertDialog loginErrorAlert(Context context, String title, String message, CFAlertDialog.CFAlertStyle cfAlertStyle) {
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                .setDialogStyle(cfAlertStyle)
                .setAutoDismissAfter(1500)
                .setTextGravity(Gravity.START)
                .setTitle(title).setIcon(R.drawable.ic_error_outline_red_900_24dp).setMessage(message);

        return builder.create();
    }

    private static class OnclickEvent implements View.OnClickListener {
        private AddToSaleListener listener;
        private EditText edtQuantity, edtPrice;
        private Inventory inventory;
        private CFAlertDialog dialog;

        private OnclickEvent(AddToSaleListener listener, EditText quantity, EditText price, Inventory inventory, CFAlertDialog dialog) {
            this.listener = listener;
            this.edtQuantity = quantity;
            this.edtPrice = price;
            this.inventory = inventory;
            this.dialog = dialog;
        }

        @Override
        public void onClick(View v) {
            String quantity = edtQuantity.getText().toString().trim(), price = edtPrice.getText().toString().trim();
            if (quantity.length() < 1 || price.length() < 1) {
                Snackbar.make(v, "Please provide both quantity and price!", Snackbar.LENGTH_SHORT).setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                }).show();
                return;
            }
            SaleItem saleItem = new SaleItem();
            saleItem.setInventory(inventory);
            saleItem.setSale(null);
            saleItem.setItemAmount(Integer.parseInt(quantity) * inventory.getSellingPrice());
            saleItem.setItemQuantity(Integer.parseInt(quantity));
            saleItem.setSellingPrice(inventory.getSellingPrice());
            listener.onItemAddedToSale(v, saleItem);
            dialog.dismiss();
            return;
        }
    }

}
