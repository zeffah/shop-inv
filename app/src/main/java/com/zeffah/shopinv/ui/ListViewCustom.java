package com.zeffah.shopinv.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ListView;


public class ListViewCustom extends ListView {
        private int oldCount = 0;
        public ListViewCustom(Context context, AttributeSet attrs)
        {
            super(context, attrs);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            android.view.ViewGroup.LayoutParams params;
            if (getCount() != oldCount) {
                int height = getChildAt(0).getHeight() + 1 ;
                oldCount = getCount();
                params = getLayoutParams();
                params.height = getCount() * height;
                setLayoutParams(params);
            }

            super.onDraw(canvas);
        }
}
