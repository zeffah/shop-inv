package com.zeffah.shopinv.ui;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.callback.ProductFetchListener;
import com.zeffah.shopinv.callback.ShopFetchListener;
import com.zeffah.shopinv.container.Container;
import com.zeffah.shopinv.pages.LoginFragment;
import com.zeffah.shopinv.utils.SessionManager;
import com.zeffah.shopinv.webservice.ProductServerAPI;
import com.zeffah.shopinv.webservice.ShopServerAPI;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by zeffah on 9/22/17.
 **/

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 5000;
    private AVLoadingIndicatorView loadingIndicatorView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        TextView txtCompanyCopyright;
        loadingIndicatorView = findViewById(R.id.avi);
        txtCompanyCopyright = findViewById(R.id.txt_company_copyright);
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int currentYear = calendar.get(Calendar.YEAR);
        String copyright = String.format(Locale.getDefault(), getString(R.string.company_copyright)+"%d Petwana ent.", currentYear);
        txtCompanyCopyright.setText(copyright);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.cfdialog_button_black_text_color));
        }

        final  boolean isUserLogged = SessionManager.isUserLoggedIn(this);
//        final boolean isUserLogged = true;
        if (isUserLogged){
            SPLASH_TIME_OUT = 1000;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadingIndicatorView.smoothToShow();
                ProductServerAPI.fetchProductsFromServer(new ProductFetchListener() {
                    @Override
                    public void onProductsFetchSuccess() {
                        ShopServerAPI.fetchAllShops(new ShopFetchListener() {
                            @Override
                            public void onShopFetchSuccess() {

                            }

                            @Override
                            public void onShopFetchFail(String message) {
                                Log.d("ErrorFetchingShops", message);
                            }
                        });
                    }

                    @Override
                    public void onProductsFetchFail(String message) {

                    }
                });

                if (isUserLogged){
                    //GoToDashboard
                    newActivityStart(Container.class);
//                    PublicMethods.newActivityStart(getApplicationContext(), Container.class, loadingIndicatorView);
                }else {
                    //GoToLogin
                    newActivityStart(LoginFragment.class);
//                    PublicMethods.newActivityStart(getApplicationContext(), LoginFragment.class, loadingIndicatorView);
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void newActivityStart(Class mClass){
        Intent intent = new Intent(getApplicationContext(), mClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        loadingIndicatorView.smoothToHide();
        finish();
    }
}
