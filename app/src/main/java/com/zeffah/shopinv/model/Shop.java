package com.zeffah.shopinv.model;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.zeffah.shopinv.modelservice.StaffService;

import java.util.List;

@Table(name = "shop", database = APPDatabase.class)
public class Shop {
    @SerializedName("id")
    @Column @PrimaryKey private long shopId;
    @SerializedName("shop_name")
    @Column private String shopName;
    @SerializedName("location")
    @Column private String shopLocation;
    @SerializedName("postal_address")
    @Column private String shopAddress;
    @SerializedName("email_address")
    @Column private String shopEmail;
    @SerializedName("town")
    @Column private String shopTown;
    @SerializedName("created_at")
    @Column private long createdAt;
    @SerializedName("opening_hour")
    @Column private long openingHour;
    @SerializedName("closing_hour")
    @Column private long closingHour;

    public List<Staff> shopStaff;

    @OneToMany(methods = OneToMany.Method.ALL, variableName = "shopStaff")
    public List<Staff> getShopStaff(){
        if (shopStaff == null){
            shopStaff = StaffService.getStaffByShopId(shopId);
        }
        return shopStaff;
    }

    public Shop() {
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLocation() {
        return shopLocation;
    }

    public void setShopLocation(String shopLocation) {
        this.shopLocation = shopLocation;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopEmail() {
        return shopEmail;
    }

    public void setShopEmail(String shopEmail) {
        this.shopEmail = shopEmail;
    }

    public String getShopTown() {
        return shopTown;
    }

    public void setShopTown(String shopTown) {
        this.shopTown = shopTown;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getOpeningHour() {
        return openingHour;
    }

    public void setOpeningHour(long openingHour) {
        this.openingHour = openingHour;
    }

    public long getClosingHour() {
        return closingHour;
    }

    public void setClosingHour(long closingHour) {
        this.closingHour = closingHour;
    }
}
