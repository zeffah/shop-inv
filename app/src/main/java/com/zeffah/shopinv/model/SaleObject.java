package com.zeffah.shopinv.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SaleObject {
    @SerializedName("saleAmount")
    private double saleAmount;
    @SerializedName("staffId")
    private long staffId;
    @SerializedName("shopId")
    private long shopId;
    @SerializedName("saleItems")
    private List<SaleItem> saleItems;

    public SaleObject(double saleAmount, long staffId, long shopId, List<SaleItem> saleItems) {
        this.saleAmount = saleAmount;
        this.staffId = staffId;
        this.shopId = shopId;
        this.saleItems = saleItems;
    }

    public double getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(double saleAmount) {
        this.saleAmount = saleAmount;
    }

    public long getStaffId() {
        return staffId;
    }

    public void setStaffId(long staffId) {
        this.staffId = staffId;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public List<SaleItem> getSaleItems() {
        return saleItems;
    }

    public void setSaleItems(List<SaleItem> saleItems) {
        this.saleItems = saleItems;
    }
}