package com.zeffah.shopinv.model;

import java.util.ArrayList;
import java.util.List;

public class TestData {

    public static List<Product> getProductList(){
        List<Product> list = new ArrayList<>();
        Product product = new Product();
        product.setProductName("Product 1");
        product.setProductManufacturer("Manufacturer 1");
        product.setProductCategory("Category 1");
        product.setDeleteStatus(0);
        product.setProductId(1);
        product.setProductSupplier("Supplier 1");
        product.setProductType("Type 1");
        if (product.save()){
         list.add(product);
        }
        return list;
    }
}
