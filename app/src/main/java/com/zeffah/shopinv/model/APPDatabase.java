package com.zeffah.shopinv.model;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = APPDatabase.NAME, version = APPDatabase.VERSION)
public class APPDatabase {
    static final String NAME = "app_db";
    static final int VERSION = 1;
}
