package com.zeffah.shopinv.model;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

@Table(database = APPDatabase.class, name = "products")
public class Product extends BaseModel {
    @Column @SerializedName("id")
    @PrimaryKey private long productId;
    @SerializedName("product_name")
    @Column private String productName;
    @SerializedName("product_supplier")
    @Column private String productSupplier;
    @SerializedName("product_category")
    @Column private String productCategory;
    @SerializedName("product_manufacturer")
    @Column private String productManufacturer;
    @SerializedName("product_type")
    @Column private String productType;
    @SerializedName("delete_status")
    @Column private int deleteStatus;

    public Product() { }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSupplier() {
        return productSupplier;
    }

    public void setProductSupplier(String productSupplier) {
        this.productSupplier = productSupplier;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductManufacturer() {
        return productManufacturer;
    }

    public void setProductManufacturer(String productManufacturer) {
        this.productManufacturer = productManufacturer;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(int deleteStatus) {
        this.deleteStatus = deleteStatus;
    }
}
