package com.zeffah.shopinv.model;

import android.graphics.drawable.Drawable;

/**
 * Created by zeffah on 11/1/16.
 **/

public class Dashboard {
    private String label;
    private int icon;
    private Drawable icons;

    public Drawable getIcons() {
        return icons;
    }

    public void setIcons(Drawable icons) {
        this.icons = icons;
    }

    public Dashboard(){}

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
