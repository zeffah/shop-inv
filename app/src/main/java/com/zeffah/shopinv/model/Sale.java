package com.zeffah.shopinv.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

@Table(name = "sales", database = APPDatabase.class)
public class Sale extends BaseModel {
   @Column
   @PrimaryKey(autoincrement = true) private long saleId;
   @Column private long createdAt;
    @Column private long updatedAt;
   @Column private String customerPhone;
   @Column private double saleAmount;
   @ForeignKey(stubbedRelationship = true)
   private Staff staff;
   public List<SaleItem> saleItems;
   public long staffId;
   public long shopId;

    @OneToMany(methods = OneToMany.Method.ALL, variableName = "saleItems")
    public List<SaleItem> getSaleItems(){
        if (saleItems == null){
            saleItems = SQLite.select().from(SaleItem.class).where(SaleItem_Table.sale_saleId.eq(saleId)).queryList();
        }
        return saleItems;
    }

    public Sale() {
    }

    public long getSaleId() {
        return saleId;
    }

    public void setSaleId(long saleId) {
        this.saleId = saleId;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public double getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(double saleAmount) {
        this.saleAmount = saleAmount;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }
}
