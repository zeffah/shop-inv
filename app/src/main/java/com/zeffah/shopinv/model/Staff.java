package com.zeffah.shopinv.model;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

@Table(name = "staff", database = APPDatabase.class)
public class Staff {
    @SerializedName("id")
    @Column @PrimaryKey private long staffId;
    @SerializedName("staff_name")
    @Column private String staffName;
    @SerializedName("phone_number")
    @Column private String  staffMobileNumber;
    @SerializedName("email_address")
    @Column private String staffEmailAddress;
    @SerializedName("national_id")
    @Column private String staffIdNumber;
    @SerializedName("username")
    @Column private String username;
    @SerializedName("password")
    @Column private String password;
    @SerializedName("shop_id")
    private int shopId;
    @ForeignKey(stubbedRelationship = true)
    private Shop shop;

    public List<Sale> sales;

    @OneToMany(methods = OneToMany.Method.ALL, variableName = "sales")
    public List<Sale> getSales(){
        if (sales == null){
            sales = SQLite.select().from(Sale.class).where(Sale_Table.staff_staffId.eq(staffId)).queryList();
        }
        return sales;
    }

    public Staff() { }

    public long getStaffId() {
        return staffId;
    }

    public void setStaffId(long staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffMobileNumber() {
        return staffMobileNumber;
    }

    public void setStaffMobileNumber(String staffMobileNumber) {
        this.staffMobileNumber = staffMobileNumber;
    }

    public String getStaffEmailAddress() {
        return staffEmailAddress;
    }

    public void setStaffEmailAddress(String staffEmailAddress) {
        this.staffEmailAddress = staffEmailAddress;
    }

    public String getStaffIdNumber() {
        return staffIdNumber;
    }

    public void setStaffIdNumber(String staffIdNumber) {
        this.staffIdNumber = staffIdNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }
}
