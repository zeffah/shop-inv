package com.zeffah.shopinv.pages;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.adapter.DashboardGridViewAdapter;
import com.zeffah.shopinv.callback.ProductFetchListener;
import com.zeffah.shopinv.callback.ShopFetchListener;
import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.model.Dashboard;
import com.zeffah.shopinv.ui.ExpandableHeightGridView;
import com.zeffah.shopinv.utils.Constants;
import com.zeffah.shopinv.utils.SessionManager;
import com.zeffah.shopinv.webservice.InventoryServerAPI;
import com.zeffah.shopinv.webservice.ProductServerAPI;
import com.zeffah.shopinv.webservice.ShopServerAPI;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {

    List<Dashboard> dashList;
    ExpandableHeightGridView gridView;
    DashboardGridViewAdapter mAdapter;
    private CarouselView carouselView;
    int[] sampleImages = {R.mipmap.locally_grown, R.mipmap.eggs, R.mipmap.animals, R.mipmap.maize, R.mipmap.petwana};

    public DashboardFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
        PublicMethods.setActionBarTitle(getActivity(), "Home", true);
        InventoryServerAPI.fetchInventoriesFromServer(SessionManager.getStaffShopId(getContext()));
    }

    @Override
    public void onResume() {
        super.onResume();
        ProductServerAPI.fetchProductsFromServer(new ProductFetchListener() {
            @Override
            public void onProductsFetchSuccess() {
                ShopServerAPI.fetchAllShops(new ShopFetchListener() {
                    @Override
                    public void onShopFetchSuccess() {
                        InventoryServerAPI.fetchInventoriesFromServer(SessionManager.getStaffShopId(getContext()));
                    }

                    @Override
                    public void onShopFetchFail(String message) {
                        Log.d("ErrorFetchingShops", message);
                    }
                });
            }

            @Override
            public void onProductsFetchFail(String message) {
                Snackbar.make(carouselView, message, Snackbar.LENGTH_LONG).show();
            }
        });
        PublicMethods.setActionBarTitle(getActivity(), "Home", true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        carouselView = view.findViewById(R.id.carouselView);
        gridView = view.findViewById(R.id.dashboard_grid_view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        carouselView.setImageListener(imageListener);
        carouselView.setPageCount(sampleImages.length);
        dashList = prepareList(view.getContext());
        initializeGridIcon(dashList);
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Picasso.with(getContext()).load(sampleImages[position]).fit().into(imageView);
        }
    };

    private void initializeGridIcon(List<Dashboard> dashboardList) {
        mAdapter = new DashboardGridViewAdapter(getActivity(), dashboardList);
        gridView.setExpanded(true);
        gridView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        gridView.setOnItemClickListener(new GridItemClickListener());
    }

    private List<Dashboard> prepareList(Context context) {
        List<Dashboard> mDashList = new ArrayList<>();
        String[] labels = context.getResources().getStringArray(R.array.dashboard_labels);
        TypedArray icons = context.getResources().obtainTypedArray(R.array.dashboard_icons);
        for (int i = 0; i < labels.length; i++) {
            Dashboard mDash = new Dashboard();
            mDash.setLabel(labels[i]);
            mDash.setIcons(icons.getDrawable(i));
            mDashList.add(mDash);
        }
        icons.recycle();
        return mDashList;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dashboard, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_logout: {
                SessionManager.logout(getContext());
                return true;
            }
            case R.id.action_inventory: {
                PublicMethods.openPageFragment(getContext(), new ShopInventoryFragment(), false);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void redirectUser(String label) {
        switch (label) {
            case Constants.MY_SALES_LIST:
                PublicMethods.openPageFragment(getActivity(), new SalesListFragment(), false);
                break;
            case Constants.MY_INVENTORY:
                PublicMethods.openPageFragment(getContext(), new ShopInventoryFragment(), false);
                break;
            case Constants.REPORTS: {
                PublicMethods.openPageFragment(getContext(), new GraphicalAnalysisFragment(), false);
            }
        }
    }

    private class GridItemClickListener implements AdapterView.OnItemClickListener {
        GridItemClickListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
            String label = ((Dashboard) adapterView.getItemAtPosition(position)).getLabel();
            redirectUser(label);
        }
    }
}
