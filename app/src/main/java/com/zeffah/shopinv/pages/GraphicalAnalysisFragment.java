package com.zeffah.shopinv.pages;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.modelservice.SaleService;
import com.zeffah.shopinv.utils.DateTimeMethods;
import com.zeffah.shopinv.utils.MyXAxisFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class GraphicalAnalysisFragment extends Fragment {
    LineChart lineChart;
    Context context;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        PublicMethods.setActionBarTitle(context, "Graphs", true);
    }

    @Override
    public void onResume() {
        super.onResume();
        PublicMethods.setActionBarTitle(context, "Graphs", true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graphical_anaysis, container, false);
        lineChart = view.findViewById(R.id.chart_line_graph);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new GraphicalAnalysis().execute();
    }

    private ArrayList<String> lineChartLabels(){
        String[] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
        ArrayList<String> xAxisLabels = new ArrayList<>();
        Collections.addAll(xAxisLabels, months);
        return xAxisLabels;
    }

    public ArrayList<Entry> lineGraphEntries(ArrayList<Double> salesPerMonth){
        ArrayList<Entry> entries = new ArrayList<>();
        for (int i = 0; i < salesPerMonth.size(); i++){
            float value = (float)(double)salesPerMonth.get(i);
            entries.add(new Entry(i, value));
        }
        return entries;
    }

    private void plotLineGraph(List<Double> salesPerMonth){
        LineDataSet lineDataSet = new LineDataSet(lineGraphEntries((ArrayList<Double>) salesPerMonth), "Sales per Month");
        lineDataSet.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
        lineDataSet.setCircleColor(ContextCompat.getColor(context, R.color.red));
        LineData lineData = new LineData(lineDataSet);
        lineData.setValueTextColor(ContextCompat.getColor(context, R.color.red));
        Description desc = lineChart.getDescription();
        String strDesc = String.format(Locale.getDefault(), context.getResources().getString(R.string.sale_for_year)+" %s", String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
        desc.setText(strDesc);
        lineChart.setData(lineData);
        lineChart.setDescription(desc);
        lineChart.invalidate();

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new MyXAxisFormatter(lineChartLabels()));
    }

    private class GraphicalAnalysis extends AsyncTask<String, Void, List<Double>> {
        final AlertDialog progress = PublicMethods.actionProgressDialog(getActivity());
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.show();
        }

        @Override
        protected List<Double> doInBackground(String... params) {
            List<Double> salesAmountPerMonth = new ArrayList<>();
            Calendar mCalendar = Calendar.getInstance();
            int thisMonth = mCalendar.get(Calendar.MONTH);
            for (int i = 0; i <= thisMonth; i++ ){
                mCalendar.add(Calendar.MONTH, -i);
                long firstDateOfMonth = DateTimeMethods.firstDayOfMonth(mCalendar.getTime());
                long lastDateOfMonth = DateTimeMethods.lastDayOfMonth(mCalendar.getTime());
                double saleInParticularMonth = SaleService.getTotalSaleAmount(String.valueOf(firstDateOfMonth), String.valueOf(lastDateOfMonth));
                salesAmountPerMonth.add(i, saleInParticularMonth);
            }
            Collections.reverse(salesAmountPerMonth);
            return salesAmountPerMonth;
        }

        @Override
        protected void onPostExecute(List<Double> salesPerMonth) {
            super.onPostExecute(salesPerMonth);
            if (progress.isShowing())
                progress.dismiss();
            if (salesPerMonth != null && !salesPerMonth.isEmpty())
                plotLineGraph(salesPerMonth);
        }
    }
}
