package com.zeffah.shopinv.pages;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zeffah.shopinv.R;
import com.zeffah.shopinv.adapter.SaleItemsListAdapter;
import com.zeffah.shopinv.callback.SaleCreateListener;
import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.model.Sale;
import com.zeffah.shopinv.model.SaleItem;
import com.zeffah.shopinv.modelservice.SaleService;

import java.io.Serializable;
import java.util.List;

public class SaleItemsListFragment extends DialogFragment implements SaleItemsListAdapter.SaleItemsListAdapterListener{
    private TextView txtSaleAmount, txtSaleDiscount;
    private AppCompatButton btnCloseSale, btnCancel;
    private List<SaleItem> saleItemList;
    private RecyclerView mRecyclerView;
    private TextView txtWarningNoStanding;
    public static final String SALE_ITEMS_LIST = "SALE_ITEMS";

    private SaleCreateListener saleCloseListener;
    public SaleItemsListFragment() {

    }

    public void setSaleCloseListener(SaleCreateListener saleCloseListener) {
        this.saleCloseListener = saleCloseListener;
    }

    public static SaleItemsListFragment newInstance(List<SaleItem> saleItems) {
        SaleItemsListFragment fragment = new SaleItemsListFragment();
        Bundle args = new Bundle();
        args.putSerializable(SALE_ITEMS_LIST, (Serializable) saleItems);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        PublicMethods.changeTitleStyle(getContext(), dialog);
        dialog.setTitle("Sale Items");
        return dialog;
    }

    @Override @SuppressWarnings("unchecked")
    public void onCreate(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_Dialog_MinWidth); /*Theme_Holo_Light_Dialog_MinWidth*/
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            saleItemList = (List<SaleItem>) getArguments().getSerializable(SALE_ITEMS_LIST);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sale_items, container, false);
        btnCancel = view.findViewById(R.id.btn_cancel_dialog);
        btnCloseSale = view.findViewById(R.id.btn_close_sale);
        txtSaleAmount = view.findViewById(R.id.txt_total_sale_amount);
        txtSaleDiscount = view.findViewById(R.id.txt_total_sale_discount);
        mRecyclerView = view.findViewById(R.id.recycle_view_sale_item_list);
        txtWarningNoStanding = view.findViewById(R.id.txt_warning_no_sale_items);
        txtWarningNoStanding.setText(String.valueOf("No list found"));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        initializeList();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnCloseSale.setOnClickListener(onClickListener);
        btnCancel.setOnClickListener(onClickListener);
    }

    private void initializeList(){
        if (!saleItemList.isEmpty()) {
            txtWarningNoStanding.setVisibility(View.GONE);
            SaleItemsListAdapter mAdapter = new SaleItemsListAdapter(saleItemList, this);
            mRecyclerView.setAdapter(mAdapter);
            txtSaleAmount.setText(String.valueOf("Ksh."+getTotalAmount(saleItemList)));
        } else {
            txtWarningNoStanding.setVisibility(View.VISIBLE);
        }
    }

    private double getTotalAmount(List<SaleItem> saleItems){
        double total = 0;
        for (SaleItem saleItem: saleItems) {
            total +=saleItem.getItemAmount();
        }
        return total;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == btnCloseSale){
                Sale sale = SaleService.getCreateSale(getActivity(), saleItemList);
                if(sale != null){
                    dismiss();
                    saleCloseListener.onCreateSale(sale);
                }
                return;
            }
            if (view == btnCancel){
                dismiss();
            }
        }
    };

    @Override
    public void onItemQuantityChange(List<SaleItem> saleItems) {
        getTotalAmount(saleItems);
    }
}
