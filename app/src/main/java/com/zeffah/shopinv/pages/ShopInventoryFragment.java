package com.zeffah.shopinv.pages;

import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.adapter.InventoryListAdapter;
import com.zeffah.shopinv.callback.AddToSaleListener;
import com.zeffah.shopinv.callback.SaleCreateListener;
import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.dialog.CustomDialog;
import com.zeffah.shopinv.model.Inventory;
import com.zeffah.shopinv.model.Sale;
import com.zeffah.shopinv.model.SaleItem;
import com.zeffah.shopinv.modelservice.SaleService;
import com.zeffah.shopinv.utils.DataManager;
import com.zeffah.shopinv.utils.SessionManager;
import com.zeffah.shopinv.webservice.SalesServerAPI;
import com.zeffah.shopinv.webservice.rest.SaleResponse;

import java.util.ArrayList;
import java.util.List;

public class ShopInventoryFragment extends Fragment implements InventoryListAdapter.InventoryListAdapterListener, SaleCreateListener {
    private List<SaleItem> saleItemList;
    private List<Inventory> inventoryList;
    private RecyclerView mRecyclerView;
    private InventoryListAdapter mAdapter;
    private TextView txtWarningNoStanding;
    private BottomNavigationView bottomNavigationView;
    private int mNotificationsCount = 0;
    private Context context;
    private AlertDialog progress;

    public ShopInventoryFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        context = getActivity();
        progress = PublicMethods.actionProgressDialog(context);
        PublicMethods.setActionBarTitle(getActivity(), "Inventory", true);
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        PublicMethods.setActionBarTitle(getActivity(), "Inventory", true);
        initializeList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inventory_list, container, false);
        mRecyclerView = view.findViewById(R.id.recycle_view_inventory_list);
        bottomNavigationView = view.findViewById(R.id.bottom_nav);
        txtWarningNoStanding = view.findViewById(R.id.txt_warning_no_standing_orders);
        txtWarningNoStanding.setText(String.valueOf("No list found"));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        saleItemList = new ArrayList<>();
        initializeList();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int itemId = item.getItemId();
                switch (itemId){
                    case R.id.action_sale_list: {
                        PublicMethods.openPageFragment(getActivity(), new SalesListFragment(), false);
                        return true;
                    }
                    case R.id.action_sale_analysis: {
                        PublicMethods.openPageFragment(getActivity(), new GraphicalAnalysisFragment(), false);
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_inventory, menu);
        MenuItem item = menu.findItem(R.id.action_go_to_sale);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        PublicMethods.setBadgeCount(context, icon, mNotificationsCount);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case R.id.action_go_to_sale: {
                if (saleItemList == null || saleItemList.size() < 1) {
                    CustomDialog.loginErrorAlert(getContext(),
                            "Empty List", "There\'s no any item in the list",
                            CFAlertDialog.CFAlertStyle.BOTTOM_SHEET).show();
                    return false;
                }
                SaleItemsListFragment saleItemsListFragment = SaleItemsListFragment.newInstance(saleItemList);
                saleItemsListFragment.setSaleCloseListener(this);
                PublicMethods.openDialogFragment(getContext(), saleItemsListFragment);
                return true;
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void initializeList() {
        inventoryList = DataManager.fetchProducts();
        if (!inventoryList.isEmpty()) {
            txtWarningNoStanding.setVisibility(View.GONE);
            mAdapter = new InventoryListAdapter(inventoryList, this);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            txtWarningNoStanding.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onIconClicked(int position, final Inventory inventory) {
        CustomDialog.addItemToSaleList(getContext(), inventory, CFAlertDialog.CFAlertStyle.BOTTOM_SHEET, new AddToSaleListener() {
            @Override
            public void onItemAddedToSale(View v, SaleItem saleItem) {
                SaleItem mnSaleItem = itemExistInList(saleItemList, saleItem);
                if (mnSaleItem != null) {
                    int existingQty = mnSaleItem.getItemQuantity();
                    int newQty = existingQty + saleItem.getItemQuantity();
                    mnSaleItem.setInventory(inventory);
                    mnSaleItem.setItemAmount(newQty * inventory.getSellingPrice());
                    mnSaleItem.setItemQuantity(newQty);
                    mnSaleItem.setSellingPrice(inventory.getSellingPrice());
                } else {
                    saleItemList.add(saleItem);
                }
                updateNotificationsBadge(saleItemList.size());
            }
        }).show();
    }

    @Override
    public void onItemRowLongClicked(int position, Inventory inventory) {

    }

    @Override
    public void onOrderStatusIconClick(int position, Inventory inventory) {

    }

    @Override
    public void onOrderApproveIconClick(int position, Inventory inventory) {

    }

    private SaleItem itemExistInList(List<SaleItem> saleItemList, SaleItem saleItem) {
        SaleItem _saleItem = null;
        for (SaleItem mnSaleItem : saleItemList) {
            if (mnSaleItem.getInventory().getInventoryId() == saleItem.getInventory().getInventoryId()) {
                _saleItem = mnSaleItem;
                break;
            }
        }
        return _saleItem;
    }

    @Override
    public void onCreateSale(Sale sale) {
        List<SaleItem> items = sale.getSaleItems();
        for (SaleItem item : items) {
            item.inventoryId = item.getInventory().getInventoryId();
        }
        sale.staffId = sale.getStaff().getStaffId();
        sale.shopId = SessionManager.getStaffShopId(getActivity());
        progress.show();
        SalesServerAPI.sendSaleToServer(sale, this);
    }

    @Override
    public void onSentToServer(SaleResponse response, Sale sale) {
        try {
            int success = response.getSuccess();
            if (success == 1) {
                if (SaleService.saveSale(sale) > 0) {
                    saleItemList = null;
                    mNotificationsCount = 0;
                    inventoryList = DataManager.fetchProducts();
                    mAdapter.reloadList(inventoryList);
                    updateNotificationsBadge(mNotificationsCount);
                    Snackbar.make(mRecyclerView, "Sale made successfully", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(mRecyclerView, "Sale not saved", Snackbar.LENGTH_LONG).show();
                }
            } else {
                Snackbar.make(mRecyclerView, "Sale not sent to server", Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        progress.cancel();
    }

    private void updateNotificationsBadge(int count) {
        mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        ((AppCompatActivity)context).invalidateOptionsMenu();
    }
}
