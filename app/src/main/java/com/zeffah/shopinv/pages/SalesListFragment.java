package com.zeffah.shopinv.pages;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.adapter.SalesExpandableRecyclerViewAdapter;
import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.model.Sale;
import com.zeffah.shopinv.model.Staff;
import com.zeffah.shopinv.modelservice.ProductService;
import com.zeffah.shopinv.utils.SessionManager;

import java.util.List;


public class SalesListFragment extends Fragment implements SalesExpandableRecyclerViewAdapter.SalesListAdapterListener {
    Staff agent;
    List<Sale> allSales;
    RecyclerView mRecyclerView;
    LinearLayoutManager layoutManager;

    TextView txtWarningNoStanding;

    SessionManager sessionManager;

    SalesExpandableRecyclerViewAdapter mAdapter;

//    private ActionModeCallback actionModeCallback;
//    private ActionMode actionMode;

    public SalesListFragment() {

    }

    public static SalesListFragment newInstance() {
        SalesListFragment fragment = new SalesListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        PublicMethods.setActionBarTitle(getActivity(), "My Sales", true);
//        sessionManager = new SessionManager(getContext());
//        agent = sessionManager.getUserDetails();
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        PublicMethods.setActionBarTitle(getActivity(), "My Sales", true);
        initializeList();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_expandable_sales_list, container, false);
        mRecyclerView = view.findViewById(R.id.recycle_view_orders_list);
        txtWarningNoStanding = view.findViewById(R.id.txt_warning_no_standing_orders);
        txtWarningNoStanding.setText("No Sales to display");
        layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        initializeList();

//        actionModeCallback = new ActionModeCallback();
        return view;
    }

    private void initializeList(){
        List<Sale> mSalesList = getAllSales();
        if (!mSalesList.isEmpty()) {
            txtWarningNoStanding.setVisibility(View.GONE);
            mAdapter = new SalesExpandableRecyclerViewAdapter(layoutManager, mSalesList, this);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            txtWarningNoStanding.setVisibility(View.VISIBLE);
        }
    }

    private List<Sale> getAllSales() {
        List<Sale> salesList = SQLite.select().from(Sale.class).queryList();
        return salesList;
    }

    @Override
    public void onIconClicked(int position, Sale sale) {
        Log.d("clickedSale", new Gson().toJson(sale));
        Toast.makeText(getActivity(), "icon click listener", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemRowLongClicked(int position, Sale sale) {
        Toast.makeText(getActivity(), "Long click listener", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onOrderStatusIconClick(final int position, final Sale sale) {
        final AlertDialog progress = PublicMethods.actionProgressDialog(getActivity());
        progress.show();
    }

    @Override
    public void onOrderApproveIconClick(int position, Sale sale) {
    }

    /*private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.edit_list_action_mode_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_delete_item:
                    // delete all the selected messages
                    deleteConfirmDialog(sentOrders, new OnDeleteListener() {
                        @Override
                        public void onDeleted() {
                            mode.finish();
                        }
                    });
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.clearSelections();
            actionMode = null;
            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.resetAnimationIndex();
                    mAdapter.notifyDataSetChanged();
                }
            });
        }
    }*/
}
