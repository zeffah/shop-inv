package com.zeffah.shopinv.pages;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.gson.Gson;
import com.zeffah.shopinv.R;
import com.zeffah.shopinv.callback.UserServerListener;
import com.zeffah.shopinv.common.PublicMethods;
import com.zeffah.shopinv.container.Container;
import com.zeffah.shopinv.dialog.CustomDialog;
import com.zeffah.shopinv.model.LoginObject;
import com.zeffah.shopinv.model.Staff;
import com.zeffah.shopinv.utils.Constants;
import com.zeffah.shopinv.utils.SessionManager;
import com.zeffah.shopinv.webservice.UserServerAPI;
import com.zeffah.shopinv.webservice.rest.APIClient;
import com.zeffah.shopinv.webservice.rest.APIInterface;
import com.zeffah.shopinv.webservice.rest.LoginResponse;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends AppCompatActivity {
    private AppCompatEditText edtUsername, edtPassword;
    private AppCompatButton btnLogin;
    private String error;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);
        this.initUIComponents();
    }

    private void initUIComponents() {
        btnLogin = findViewById(R.id.btn_login);
        edtPassword = findViewById(R.id.edt_password);
        edtUsername = findViewById(R.id.edt_username);

        btnLogin.setOnClickListener(clickListener);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            if (v == btnLogin) {
                String username = edtUsername.getText().toString().trim();
                String password = edtPassword.getText().toString().trim();
                if (!validateViews(username, password)) {
                    Snackbar.make(v, error, Snackbar.LENGTH_LONG).setAction("", null).show();
//                    CustomDialog.loginErrorAlert(LoginFragment.this, "Login error", error, CFAlertDialog.CFAlertStyle.BOTTOM_SHEET).show();
                    return;
                }
                final AlertDialog progress = PublicMethods.actionProgressDialog(LoginFragment.this);
                progress.show();
                UserServerAPI.userLogin(username, password, new UserServerListener.USerLoginListener() {
                    @Override
                    public void onResponseSuccess(Staff staff) {
                        boolean sessionCreated = SessionManager.setUserSession(LoginFragment.this, staff);
                        progress.dismiss();
                        if (sessionCreated) {
                            newActivityStart(Container.class);
                        } else {
                            Snackbar.make(v, "An error occurred while creating session. Try again", Snackbar.LENGTH_LONG).setAction("", null).show();
                        }
                    }

                    @Override
                    public void onResponseError(String msg) {
                        progress.dismiss();
                        Snackbar.make(v, msg, Snackbar.LENGTH_LONG).setAction("", null).show();
                    }

                    @Override
                    public void onThrowError(String msg) {
                        progress.dismiss();
                        Snackbar.make(v, msg, Snackbar.LENGTH_LONG).setAction("", null).show();
                    }
                });
            }
        }
    };

    private boolean validateViews(String username, String password) {
        if (username.length() < 1) {
            error = String.valueOf("Please enter username");
            return false;
        }
        if (password.length() < 1) {
            error = String.valueOf("Please enter password");
            return false;
        }
        if (password.length() < 8) {
            error = String.valueOf("Password too short. Must be at least 8 character long");
            return false;
        }
        return true;
    }

    private void newActivityStart(Class nextClass) {
        Intent intent = new Intent(getApplicationContext(), nextClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
