package com.zeffah.shopinv.modelservice;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.ModelAdapter;
import com.zeffah.shopinv.model.Shop;
import com.zeffah.shopinv.model.Staff;
import com.zeffah.shopinv.model.Staff_Table;

import java.util.List;

public class StaffService {

    public static boolean saveStaffToDB(Staff staff){
        ModelAdapter<Staff> mnStaff = FlowManager.getModelAdapter(Staff.class);
        return mnStaff.save(staff);
    }

    public static List<Staff> getStaffByShopId(long shopId) {
        return SQLite.select().from(Staff.class).where(Staff_Table.shop_shopId.eq(shopId)).queryList();
    }
    public static Staff getStaffById(long staffId) {
        return SQLite.select().from(Staff.class).where(Staff_Table.staffId.eq(staffId)).querySingle();
    }
}
