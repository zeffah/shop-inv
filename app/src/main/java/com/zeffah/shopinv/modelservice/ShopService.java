package com.zeffah.shopinv.modelservice;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.zeffah.shopinv.model.Shop;
import com.zeffah.shopinv.model.Shop_Table;

public class ShopService {
    public static Shop getShopById(long shopId) {
        return SQLite.select().from(Shop.class).where(Shop_Table.shopId.eq(shopId)).querySingle();
    }
}
