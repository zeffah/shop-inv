package com.zeffah.shopinv.modelservice;

import android.content.Context;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.FlowCursor;
import com.zeffah.shopinv.model.APPDatabase;
import com.zeffah.shopinv.model.Inventory;
import com.zeffah.shopinv.model.Sale;
import com.zeffah.shopinv.model.SaleItem;
import com.zeffah.shopinv.utils.DateTimeMethods;
import com.zeffah.shopinv.utils.SessionManager;

import java.util.List;

public class SaleService {
    public static long createSale(Context context, List<SaleItem> saleItems) {
        Sale sale = new Sale();
        sale.setStaff(SessionManager.getSessionStaff(context));
        sale.setCreatedAt(DateTimeMethods.getCurrentTime());
        sale.setUpdatedAt(DateTimeMethods.getCurrentTime());
        sale.setCustomerPhone(null);
        sale.setSaleAmount(getSaleAmount(saleItems));
        sale.saleItems = saleItems;
        if(sale.save()){
            for (SaleItem saleItem: saleItems){
                saleItem.setSale(sale);
                if (saleItem.save()){
                    Inventory inventory = InventoryService.getInventoryById(saleItem.getInventory().getInventoryId());
                    if (inventory != null){
                        inventory.setInventoryQuantity(inventory.getInventoryQuantity()-saleItem.getItemQuantity());
                        inventory.save();
                    }
                }
            }
            return sale.getSaleId();
        }
        return 0;
    }

    public static long saveSale(Sale sale) {
        if(sale.save()){
            for (SaleItem saleItem: sale.saleItems){
                saleItem.setSale(sale);
                if (saleItem.save()){
                    Inventory inventory = InventoryService.getInventoryById(saleItem.getInventory().getInventoryId());
                    if (inventory != null){
                        inventory.setInventoryQuantity(inventory.getInventoryQuantity()-saleItem.getItemQuantity());
                        inventory.save();
                    }
                }
            }
            return sale.getSaleId();
        }
        return 0;
    }

    public static Sale getCreateSale(Context context, List<SaleItem> saleItems) {
        Sale sale = new Sale();
        sale.setStaff(SessionManager.getSessionStaff(context));
        sale.setCreatedAt(DateTimeMethods.getCurrentTime());
        sale.setUpdatedAt(DateTimeMethods.getCurrentTime());
        sale.setCustomerPhone(null);
        sale.setSaleAmount(getSaleAmount(saleItems));
        sale.saleItems = saleItems;

        return sale;
    }

    private static double getSaleAmount(List<SaleItem> items){
        double amount = 0;
        for (SaleItem item: items){
            amount+=item.getItemAmount();
        }
        return amount;
    }

    public static double getTotalSaleAmount(String... datePeriods) {
        double totalSales = 0.0;
        try {
            FlowCursor cursor = FlowManager.getWritableDatabase(APPDatabase.class)
                    .rawQuery("SELECT SUM(saleAmount) FROM SALES WHERE createdAt >= ? and createdAt <= ? ", datePeriods);
            if (cursor != null && cursor.moveToNext()) {
                totalSales = cursor.getLongOrDefault(0);
                cursor.close();
            }
            return totalSales;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return totalSales;
    }
}
