package com.zeffah.shopinv.modelservice;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.zeffah.shopinv.model.Inventory;
import com.zeffah.shopinv.model.Inventory_Table;

public class InventoryService {
    public static Inventory getInventoryById(long inventoryId) {
        return SQLite.select().from(Inventory.class).where(Inventory_Table.inventoryId.eq((int)inventoryId)).querySingle();
    }
}
