package com.zeffah.shopinv.modelservice;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.zeffah.shopinv.model.Product;
import com.zeffah.shopinv.model.Product_Table;

public class ProductService {
    public static Product getProductById(long productId) {
        return SQLite.select().from(Product.class).where(Product_Table.productId.eq(productId)).querySingle();
    }
}
